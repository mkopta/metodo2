<?php

define("INDEX", "1");

$rv = @include("configuration.php");
if ( ! $rv )
{
	echo "<h1>Can not load configuration!</h1> \n";
	echo "<p>Please, check file 'configuration.php' existence\n";
	echo "and make sure it is well formed.</p><p>In case of trouble, \n";
	echo "follow the documentation in directory <a href=\"";
	echo "doc/\">doc/</a>.</p>";
	die();
}

$cookie = new cCookie(COOKIE_NAME);

if ( $cookie->isGiven() )
{
	$class = "ccCookieLover";
}
else // if cookie isn't given
{
	$class = "ccMrNoCandy";
}

$controller = new $class (COOKIE_NAME, APP_STYLEDIR, APP_STYLE,
	DB_SERVER, DB_DATABASE, DB_USERNAME, DB_PASSWORD);

$controller->start();

function __autoload ( $class )
{
	if ( $class[0] == "c" && $class[1] == "c" ) // Class & Controller
	{
		require("classes/controllers/" . $class . ".php");
	}
	else
	{
		require ("classes/" . $class . ".php");
	}
}

?>
