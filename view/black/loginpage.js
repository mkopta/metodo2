// Javascript for loginpage of metodo2

function validate()
{
	// our celebrities
	var username = document.getElementById("usernameField");
	var password = document.getElementById("passwordField");
	var errplace = document.getElementById("errors");

	// clean error place
	if ( errplace.hasChildNodes() )
	{
		while ( errplace.childNodes.length >= 1 )
		{
			errplace.removeChild( errplace.firstChild );       
		} 
	}

	// reset error colors
	username.style.backgroundColor = 'black';
	password.style.backgroundColor = 'black';

	if ( username.value == '' )
	{
		username.style.backgroundColor = '#f66';
		username.focus();

		var atr1 = document.createAttribute("class");
		atr1.nodeValue = "error";
		var atr2 = document.createAttribute("id");
		atr2.nodeValue = "errorText";

		var err = document.createElement('p');
		err.innerHTML = 'You have to fill username!';
		err.setAttributeNode(atr1);
		err.setAttributeNode(atr2);
		errplace.appendChild(err);

		//parent.appendChild(element);
		//parent.removeChild(element);

		return false;
	}

	if ( password.value == '' )
	{
		password.style.backgroundColor = '#f66';
		password.focus();

		var atr1 = document.createAttribute("class");
		atr1.nodeValue = "error";
		var atr2 = document.createAttribute("id");
		atr2.nodeValue = "errorText";

		var err = document.createElement('p');
		err.innerHTML = 'You have to fill password!';
		err.setAttributeNode(atr1);
		err.setAttributeNode(atr2);
		errplace.appendChild(err);

		return false;
	}

	return true;
}

function pageload()
{
	document.getElementById("usernameField").focus();
}

if (window.addEventListener)
{ window.addEventListener("load", pageload, false); }
else if (window.attachEvent)
{ window.attachEvent("onload", pageload); }


// EOF
