<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<?php function it($n){for($i=0;$i<$n;$i++)echo"\t";} ?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">  
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta http-equiv="content-language" content="en" />
		<meta name="author" content="Martin 'dum8d0g' Kopta http://martin.kopta.eu/" />
		<meta name="keywords" content="metodo2, webtodo, web, todo, poznámky, úkoly, tasks" />
		<meta name="description" content="Web todo organizer" />
		<meta name="robots" content="ALL,FOLLOW" />
		<meta name="cache" content="no-cache" />
		<link rel="stylesheet" type="text/css" href="<?php echo $prefix; ?>mainpage.css" />
		<link rel="icon" type="image/x-icon" href="<?php echo $prefix; ?>favico.png" />
		<script type="text/javascript" src="<?php echo $prefix; ?>plugin.js"></script>
		<script type="text/javascript" src="<?php echo $prefix; ?>mainpage.js"></script>
		<title><?php echo htmlspecialchars($pageTitle); ?></title>
	</head>
	<body>
		<div id="content">

			<div id="left">
				<div id="left-head">
					<a href="<?php
					echo $_SERVER['PHP_SELF'];
					?>">
						<img src="<?php echo $prefix; ?>logo.png" alt="Metodo2 logo" />
					</a>
				</div>

				<div id="left-content">
					<?php
					echo "Hi ".htmlspecialchars($username);
					echo "! You have ".$pendingTodosSum;
					echo " things to do.\n";
					?>
					<br />
					<br />
					(<a href="<?php
					echo $_SERVER['PHP_SELF'];
					?>?do=passwd">Change Password</a>)
					(<a href="doc/">Documentation</a>)
					(<a href="<?php
					echo $_SERVER['PHP_SELF'];
					?>?do=logout">Logout</a>)

					<div id="todoform">
						<form action="<?php
						echo $_SERVER['PHP_SELF'];
						?>" method="post" onsubmit="return validate();">
							<fieldset>
								<legend>Add new todo</legend>
								<input type="hidden" name="update" value="<?php
								if ( isset ( $idtodo ) )
								{
									echo $idtodo;
								}
								else
								{
									echo "-1";
								}
								?>" />

								<div id="errors">
								<?php
								if ( isset ( $addTodoError ) )
								{
									echo "<p class=\"error\">\n";
									echo it(9).$addTodoError."\n";
									echo it(8)."</p>\n";
								}
								?>
								</div>
								
								<label>Priority</label>
								<select tabindex="1" name="prioritySelect">
								<?php
								if ( ! isset ( $priority ) )
								{ $priority = 3; }
								echo "\n";
								for ( $i = 1; $i < 6; $i++ )
								{
									echo it(9)."<option ";
									if ( $i == $priority )
									{ echo "selected=\"selected\" "; }
									echo "value=\"".$i."\">".$i." (";
									switch ( $i )
									{
										case '1': echo "very low"; break;
										case '2': echo "low"; break;
										case '3': echo "medium"; break;
										case '4': echo "high"; break;
										case '5': echo "critical"; break;
									}
									echo ")</option>\n";
								}
								echo "\n";
								?>
								</select>
								
								<br />
								<label>Deadline</label>
								<select tabindex="2" name="dlY">
									<option value="---">---</option>
								<?php
								echo "\n";
								$actualYear  = date ("Y");
								if ( ! isset ( $selectedYear ) )
								{ $selectedYear = -1; }
								if ( ! isset ( $selectedMonth ) )
								{ $selectedMonth = -1; }
								if ( ! isset ( $selectedDay ) )
								{ $selectedDay = -1; }

								for ( $i = $actualYear; $i < $actualYear+20; $i++ )
								{
									echo it(9)."<option ";
									if ( $i == $selectedYear )
									{ echo "selected=\"selected\" "; }
									echo "value=\"".$i."\">".$i."</option>\n";
								}
								echo "\n";
								?>
								</select> <!-- dlY //-->

								<select tabindex="3" name="dlM">
									<option value="---">---</option>
								<?php
								echo "\n";
								for ( $i = 1; $i < 13; $i++ )
								{
									echo it(9)."<option ";
									if ( $i == $selectedMonth )
									{ echo "selected=\"selected\" "; }
									echo "value=\"".$i."\">".$i."</option>\n";
								}
								echo "\n";
								?>
								</select> <!-- dlM //-->

								<select tabindex="4" name="dlD">
									<option value="---">---</option>
								<?php
								echo "\n";
								/* FIXME 30 or 31??
								 * what the hell.. 
								 */
								for ( $i = 1; $i < 32; $i++ )
								{
									echo it(9)."<option ";
									if ( $i == $selectedDay )
									{ echo "selected=\"selected\" "; }
									echo "value=\"".$i."\">".$i."</option>\n";
								}
								echo "\n";
								?>
								</select> <!-- dlD //-->
								<br />


								<label>Labels</label> <br />
								<input tabindex="5" name="labelsField" value="<?php
								if ( isset ( $labelsField ) ) 
								{
									echo htmlspecialchars ( $labelsField ) ;
								}
								?>"/>
								<br />

								<label>Text</label> <br />
								<textarea
								          id="todoTextArea"
								          name="todoTextArea"
								          rows="4"
								          cols="35"
								          tabindex="6"
								><?php
								if ( isset ( $todoTextArea ) )
								{
									echo htmlspecialchars( $todoTextArea );
								}
								?></textarea>

								<br />
								<input
								       type="submit"
								       name="addTodo"
								       value="<?php
									   if ( !isset($idtodo) )
									   {
										   echo "Add todo";
									   }
									   else if ( $idtodo == -1 )
									   {
										   echo "Add todo";
									   }
									   else
									   {
										   echo "Modify todo";
									   }
									   ?>"
								       tabindex="7"
								/>
							</fieldset>
						</form>
					</div>



					<br />
					<form action="">
						<fieldset>
								<legend>Filters</legend>
								<ul>
									<li>
										Filter by priority: <br />
										<a href="<?php echo $_SERVER['PHP_SELF']; ?>?do=filter&amp;by=priority&amp;level=1">1 (very low)</a>
										<a href="<?php echo $_SERVER['PHP_SELF']; ?>?do=filter&amp;by=priority&amp;level=2">2 (low)</a>
										<a href="<?php echo $_SERVER['PHP_SELF']; ?>?do=filter&amp;by=priority&amp;level=3">3 (medium)</a>
										<a href="<?php echo $_SERVER['PHP_SELF']; ?>?do=filter&amp;by=priority&amp;level=4">4 (high)</a>
										<a href="<?php echo $_SERVER['PHP_SELF']; ?>?do=filter&amp;by=priority&amp;level=5">5 (critical)</a>
									</li>
									<?php
									if ( isset( $labels ) )
									{
										echo "<li>";
										echo "Filter by label: \n";
										foreach ( $labels as $id=>$label )
										{
											echo it(10)."<a href=\"";
											echo $_SERVER['PHP_SELF'];
											echo "?do=filter&amp;by=label&amp;id=";
											echo $id."\">".$label."</a>, \n";
										}
										echo it(9)."</li>\n";
									}
									?>
									<li>
										<a href="<?php echo $_SERVER['PHP_SELF'];
										?>?do=filter&amp;by=undone">Show unfinished todos</a>
									</li>
									<li>
										<a href="<?php echo $_SERVER['PHP_SELF'];
										?>?do=filter&amp;by=done">Show finished todos</a>
									</li>
									<li>
										<a href="<?php echo $_SERVER['PHP_SELF']; ?>?do=reset">Reset</a>
									</li>
								</ul>
						</fieldset>
					</form>

				</div>

			</div>

			<div id="right">
				<?php 
				echo "\n";
				if ( isset($listOfTodos) && !empty($listOfTodos) )
				{
					$y = date ("Y");
					$m = date ("m");
					$d = date ("d") + 3;

					foreach ( $listOfTodos as $todo )
					{
						/* Check if this todo is after deadline or near it */
						$warning = 0;
						if ( ! $todo['t_done'] )
						{
							if ( isset ($todo['deadline']) )
							{
								$dl = explode ("-", $todo['deadline']);
								if (
										($dl[0]<$y) or                  // deadline before actual year
										($dl[0]==$y and $dl[1]<$m) or    // deadline before actual month
										/* deadline today or three days forward */
										($dl[0]==$y and $dl[1]==$m and $dl[2]<=$d )
								   )
								{
									$warning = 1;
								}
							}
						}

						/* Create html output */
						echo it(4)."<div class=\"todo\">\n";
						echo it(5)."<div class=\"todohead\">\n";
						echo it(6);
						echo "<img src=\"";
						echo $prefix."prio".$todo['priority'];
						echo ".png\" alt=\"priority\"/>\n";
						echo it(6)."Entered: ".$todo['enteredDATE']."\n";
						if ( isset($todo['deadline']) )
						{
							echo it(6)."Deadline: ".$todo['deadline']."\n";
						}
						echo it(5)."</div>\n";
						if ( $warning )
						{
							echo it(5)."<div class=\"todolabels-dl\">\n";
						}
						else
						{
							echo it(5)."<div class=\"todolabels\">\n";
						}
						echo it(6).htmlspecialchars($todo['labels'])."\n";
						echo it(5)."</div>\n";
						if ( ! $todo['t_done'] )
						{
							if ( $warning )
							{
								echo it(5)."<div class=\"todotext-dl\">\n";
							}
							else
							{
								echo it(5)."<div class=\"todotext\">\n";
							}
						}
						else
						{
							echo it(5)."<div class=\"donetodotext\">\n";
						}
						echo it(6).htmlspecialchars($todo['text'])."\n";
						echo it(5)."</div>\n";
						if ( $warning )
						{
							echo it(5)."<div class=\"todoactions-dl\">\n";
						}
						else
						{
							echo it(5)."<div class=\"todoactions\">\n";
						}
						echo it(6)."<a href=\"";
						echo $_SERVER['PHP_SELF']."?do=modify&amp;idtodo=";
						echo $todo['idtodo'];
						echo "\">Modify</a>\n";
						echo it(6)."<a href=\"";
						if ( $todo['t_done'] == 0 )
						{
							echo $_SERVER['PHP_SELF']."?do=markdone&amp;idtodo=";
							echo $todo['idtodo'];
							echo "\">Mark as done</a>\n";
						}
						else
						{
							echo $_SERVER['PHP_SELF']."?do=markundone&amp;idtodo=";
							echo $todo['idtodo'];
							echo "\">Mark as undone</a>\n";
						}
						echo it(6)."<a href=\"";
						echo $_SERVER['PHP_SELF']."?do=remove&amp;idtodo=";
						echo $todo['idtodo'];
						echo "\">Remove</a>\n";
						echo it(5)."</div>\n";
						echo it(4)."</div>\n";
						echo "\n";
					}
				}
				else
				{
					if ( ! isset($_GET['do']) )
					{
						echo it(4)."<p class=\"haiku\">\n";
						echo it(5)."You had to do it<br />\n";
						echo it(5)."You did it<br />\n";
						echo it(5)."It is done<br /><br />\n";
						echo it(5)."So real<br />\n";
						echo it(4)."</p>\n";
					}
					else
					{
						echo it(4)."No results.\n";
					}
				}
				?>
			</div>

			<div class="cleaner"></div>

			<div id="bottom">
				<?php echo $bottomText."\n"; ?>
			</div>

		</div>
	</body>
</html>

