// Javascript for registration form of metodo2

function validate()
{
	/* 
	 * Variables
	 */
	var x = true; // return value
	var username    = document.getElementById("usernameField");
	var password    = document.getElementById("passwordField");
	var password2   = document.getElementById("passwordField2");
	var antisp1     = document.getElementById("antisp1");
	var antisp2     = document.getElementById("antisp2");
	var antisp3     = document.getElementById("antisp3");
	var usernameRE  = /^[-_a-z0-9]{2,32}$/;
	var passwordRE  = /^.{4,50}$/;
	var antisp1RE   = /^[rR][eE][dD]$/;
	var antisp2RE   = /^[nN][oO]+[!]*$/;
	var antisp3RE   = /^[cC][aA][kK][eE][!]*$/;

	/*
	 * Reset error colors
	 */
	username.style.backgroundColor  = 'black';
	password.style.backgroundColor  = 'black';
	password2.style.backgroundColor = 'black';
	antisp1.style.backgroundColor   = 'black';
	antisp2.style.backgroundColor   = 'black';
	antisp3.style.backgroundColor   = 'black';

	/*
	 * Clear old errors
	 */
	removeAllChilds(document.getElementById("usernameFieldError"));
	removeAllChilds(document.getElementById("passwordFieldError"));
	removeAllChilds(document.getElementById("passwordFieldError2"));
	removeAllChilds(document.getElementById("antisp1Error"));
	removeAllChilds(document.getElementById("antisp2Error"));
	removeAllChilds(document.getElementById("antisp3Error"));

	/* 
	 * Tests
	 */
	if ( password2.value == '' )
	{
		password2.style.backgroundColor = '#f66';
		password2.focus();
		var errplace = document.getElementById("passwordFieldError2");
		var atr1     = document.createAttribute("class");
		var atr2     = document.createAttribute("id");
		var err      = document.createElement('p');
		atr1.nodeValue = "error";
		atr2.nodeValue = "errorText";
		err.innerHTML = 'You have to fill the password twice!';
		err.setAttributeNode(atr1);
		err.setAttributeNode(atr2);
		errplace.appendChild(err);
		x = false;
	}
	if ( password.value == '' )
	{
		password.style.backgroundColor = '#f66';
		password.focus();
		var errplace = document.getElementById("passwordFieldError");
		var atr1     = document.createAttribute("class");
		var atr2     = document.createAttribute("id");
		var err      = document.createElement('p');
		atr1.nodeValue = "error";
		atr2.nodeValue = "errorText";
		err.innerHTML = 'You have to fill the password!';
		err.setAttributeNode(atr1);
		err.setAttributeNode(atr2);
		errplace.appendChild(err);
		x = false;
	}
	if ( password2.value != '' && password.value != '' &&
		 password.value != password2.value )
	{
		password.style.backgroundColor = '#f66';
		password2.style.backgroundColor = '#f66';
		password.select();
		password.focus();
		var errplace = document.getElementById("passwordFieldError");
		var errplace2= document.getElementById("passwordFieldError2");
		var atr1     = document.createAttribute("class");
		var atr2     = document.createAttribute("id");
		var err      = document.createElement('p');
		atr1.nodeValue = "error";
		atr2.nodeValue = "errorText";
		err.innerHTML = 'Passwords does not match eatch other!';
		err.setAttributeNode(atr1);
		err.setAttributeNode(atr2);
		errplace.appendChild(err);
		errplace2.appendChild(err);
		x = false;
	}
	else if ( password2.value != '' && password.value != '' &&
		 password.value == password2.value )
	{
		if ( passwordRE.test(password.value) == false )
		{
			password.style.backgroundColor = '#f66';
			password.focus();
			password.select();
			var errplace = document.getElementById("passwordFieldError");
			var atr1     = document.createAttribute("class");
			var atr2     = document.createAttribute("id");
			var err      = document.createElement('p');
			atr1.nodeValue = "error";
			atr2.nodeValue = "errorText";
			err.innerHTML = 'Password must be from at least 4 characters long (max 50).';
			err.setAttributeNode(atr1);
			err.setAttributeNode(atr2);
			errplace.appendChild(err);
			x = false;
		}
	}
	if ( username.value == '' )
	{
		username.style.backgroundColor = '#f66';
		username.focus();
		var errplace = document.getElementById("usernameFieldError");
		var atr1     = document.createAttribute("class");
		var atr2     = document.createAttribute("id");
		var err      = document.createElement('p');
		atr1.nodeValue = "error";
		atr2.nodeValue = "errorText";
		err.innerHTML = 'You have to fill the username!';
		err.setAttributeNode(atr1);
		err.setAttributeNode(atr2);
		errplace.appendChild(err);
		x = false;
	}
	else
	{
		if ( usernameRE.test(username.value) == false )
		{
			username.style.backgroundColor = '#f66';
			username.focus();
			username.select();
			var errplace = document.getElementById("usernameFieldError");
			var atr1     = document.createAttribute("class");
			var atr2     = document.createAttribute("id");
			var err      = document.createElement('p');
			atr1.nodeValue = "error";
			atr2.nodeValue = "errorText";
			err.innerHTML = 'Username must be from 2 to '+
			'32 characters long and can contain only '+
			'alphanumeric characters and "_" and "-".';
			err.setAttributeNode(atr1);
			err.setAttributeNode(atr2);
			errplace.appendChild(err);
			x = false;
		}
	}

	if ( antisp3.value == '' )
	{
		antisp3.style.backgroundColor = '#f66';
		antisp3.focus();
		var errplace = document.getElementById("antisp3Error");
		var atr1     = document.createAttribute("class");
		var atr2     = document.createAttribute("id");
		var err      = document.createElement('p');
		atr1.nodeValue = "error";
		atr2.nodeValue = "errorText";
		err.innerHTML = 'You did not aswered the question!';
		err.setAttributeNode(atr1);
		err.setAttributeNode(atr2);
		errplace.appendChild(err);
		x = false;
	}
	else if ( antisp3RE.test(antisp3.value) == false ) 
	{
		antisp3.style.backgroundColor = '#f66';
		antisp3.focus();
		antisp3.select();
		var errplace = document.getElementById("antisp3Error");
		var atr1     = document.createAttribute("class");
		var atr2     = document.createAttribute("id");
		var err      = document.createElement('p');
		atr1.nodeValue = "error";
		atr2.nodeValue = "errorText";
		err.innerHTML = 'You did not aswered the question right!';
		err.setAttributeNode(atr1);
		err.setAttributeNode(atr2);
		errplace.appendChild(err);
		x = false;
	}

	if ( antisp2.value == '' )
	{
		antisp2.style.backgroundColor = '#f66';
		antisp2.focus();
		var errplace = document.getElementById("antisp2Error");
		var atr1     = document.createAttribute("class");
		var atr2     = document.createAttribute("id");
		var err      = document.createElement('p');
		atr1.nodeValue = "error";
		atr2.nodeValue = "errorText";
		err.innerHTML = 'You did not aswered the question!';
		err.setAttributeNode(atr1);
		err.setAttributeNode(atr2);
		errplace.appendChild(err);
		x = false;
	}
	else if ( antisp2RE.test(antisp2.value) == false ) 
	{
		antisp2.style.backgroundColor = '#f66';
		antisp2.focus();
		antisp2.select();
		var errplace = document.getElementById("antisp2Error");
		var atr1     = document.createAttribute("class");
		var atr2     = document.createAttribute("id");
		var err      = document.createElement('p');
		atr1.nodeValue = "error";
		atr2.nodeValue = "errorText";
		err.innerHTML = 'You did not aswered the question right!';
		err.setAttributeNode(atr1);
		err.setAttributeNode(atr2);
		errplace.appendChild(err);
		x = false;
	}

	if ( antisp1.value == '' )
	{
		antisp1.style.backgroundColor = '#f66';
		antisp1.focus();
		var errplace = document.getElementById("antisp1Error");
		var atr1     = document.createAttribute("class");
		var atr2     = document.createAttribute("id");
		var err      = document.createElement('p');
		atr1.nodeValue = "error";
		atr2.nodeValue = "errorText";
		err.innerHTML = 'You did not aswered the question!';
		err.setAttributeNode(atr1);
		err.setAttributeNode(atr2);
		errplace.appendChild(err);
		x = false;
	}
	else if ( antisp1RE.test(antisp1.value) == false ) 
	{
		antisp1.style.backgroundColor = '#f66';
		antisp1.focus();
		antisp1.select();
		var errplace = document.getElementById("antisp1Error");
		var atr1     = document.createAttribute("class");
		var atr2     = document.createAttribute("id");
		var err      = document.createElement('p');
		atr1.nodeValue = "error";
		atr2.nodeValue = "errorText";
		err.innerHTML = 'You did not aswered the question right!';
		err.setAttributeNode(atr1);
		err.setAttributeNode(atr2);
		errplace.appendChild(err);
		x = false;
	}

	return x;
}

function removeAllChilds ( element )
{
	if ( element.hasChildNodes() )
	{
		while ( element.childNodes.length >= 1 )
		{
			element.removeChild( element.firstChild );       
		} 
	}
}

function pageload()
{
	document.getElementById("usernameField").focus();
}

if (window.addEventListener)
{ window.addEventListener("load", pageload, false); }
else if (window.attachEvent)
{ window.attachEvent("onload", pageload); }

// EOF
