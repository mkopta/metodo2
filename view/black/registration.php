<?php
if (!defined("INDEX"))
{
	// This file is called in wrong way!
	echo "You probably don't have to be here. Please, go back.";
	die();
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">  
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta http-equiv="content-language" content="en" />
		<meta name="author" content="Martin 'dum8d0g' Kopta http://martin.kopta.eu/" />
		<meta name="keywords" content="metodo2, webtodo, web, todo, poznámky, úkoly, tasks" />
		<meta name="description" content="Web todo organizer" />
		<meta name="robots" content="ALL,FOLLOW" />
		<meta name="cache" content="no-cache" />
		<link rel="stylesheet" type="text/css" href="<?php echo $prefix; ?>registration.css" />
		<link rel="icon" type="image/x-icon" href="<?php echo $prefix; ?>favico.png" />
		<script type="text/javascript" src="<?php echo $prefix; ?>registration.js"></script>
		<title><?php echo htmlspecialchars($pageTitle); ?></title>
	</head>
	<body>
		<div id="content">

			<div id="head">
				<a href=""><img src="<?php echo $prefix; ?>logo.png" alt="Metodo2 logo" /></a>
			</div>

			<div id="middle">
				<div id="form">
					<form action="<?php
					echo $_SERVER['PHP_SELF'];
					?>" method="post" onsubmit="return validate();">
						<fieldset>
							<legend>Registration</legend>
							<p>Note: fields with asterisk (*) are required fields.</p>
							<fieldset>
								<legend>Username and Password</legend>
								<label for="usernameField" class="needed">
									* Username
								</label>
								<input
								       tabindex="1"
								       type="text"
								       id="usernameField"
								       name="usernameField"
								       value="<?php
								if (isset($usernameField))
								{
									echo htmlspecialchars($usernameField);
								} ?>"
								/><br />
								<div id="usernameFieldError">
								<?php
								if (isset($usernameFieldError))
								{
									echo "<p class=\"error\">";
									echo $usernameFieldError;
									echo "</p>";
								}
								echo "\n";
								?>
								</div>
								<label for="passwordField" class="needed">
									* Password
								</label>
								<input
								       tabindex="2"
								       type="password"
								       id="passwordField"
								       name="passwordField"
								       value=""
								/><br />
								<div id="passwordFieldError">
								<?php
								if (isset($passwordFieldError))
								{
									echo "<p class=\"error\">";
									echo $passwordFieldError;
									echo "</p>";
								}
								echo "\n";
								?>
								</div>
								<label for="passwordField2" class="needed">
									* Password again:
								</label>
								<input
								       tabindex="3"
								       type="password"
								       id="passwordField2"
								       name="passwordField2"
								       value=""
								/><br />
								<div id="passwordFieldError2">
								<?php
								if (isset($passwordFieldError))
								{
									echo "<p class=\"error\">";
									echo $passwordFieldError;
									echo "</p>";
								}
								echo "\n";
								?>
								</div>
							</fieldset>
							<fieldset>
								<legend>Personal informations</legend>
								<label for="firstnameField" class="optional">
									First name
								</label>
								<input
								       tabindex="4"
								       type="text"
								       id="firstnameField"
								       name="firstnameField"
								       value="<?php
								if (isset($firstnameField))
								{
									echo htmlspecialchars($firstnameField);
								}
								?>"
								/><br />
								<label for="lastnameField" class="optional">
									Last name
								</label>
								<input tabindex="5"
								       type="text"
								       id="lastnameField"
								       name="lastnameField"
								       value="<?php
								if (isset($lastnameField))
								{
									echo htmlspecialchars($lastnameField);
								}
								?>"
								/><br />
								<label for="contactField" class="optional">
									Contact
								</label>
								<input tabindex="6"
								       type="text"
								       id="contactField"
								       name="contactField"
								       value="<?php
								if (isset($contactField))
								{
									echo htmlspecialchars($contactField);
								}
								?>"
								/><br />
							</fieldset>
							<fieldset>
								<legend>Antispam check</legend>
								<label for="antisp1" class="needed">
									* Which color has a red baloon?
								</label>
								<input
								       tabindex="7"
								       type="text"
								       id="antisp1"
								       name="antisp1"
								       value="<?php
								if ( isset($antisp1) )
								{
									echo htmlspecialchars($antisp1);
								}
								?>"
								/><br />
								<div id="antisp1Error">
								<?php
								if ( isset($antisp1Error) )
								{
									echo "<p class=\"error\">";
									echo $antisp1Error;
									echo "</p>";
								}
								echo "\n";
								?>
								</div>
								<label for="antisp2" class="needed">
									* Are you currently on planet Mars? [yes/no]
								</label>
								<input
								       tabindex="8"
								       type="text"
								       id="antisp2"
								       name="antisp2"
								       value="<?php
								if ( isset($antisp2) )
								{
									echo htmlspecialchars($antisp2);
								}
								?>"
								/><br />
								<div id="antisp2Error">
								<?php
								if ( isset($antisp2Error) )
								{
									echo "<p class=\"error\">";
									echo $antisp2Error;
									echo "</p>";
								}
								echo "\n";
								?>
								</div>
								<label for="antisp3" class="needed">
									* Do you prefer to eat a cake or a rock?
								</label>
								<input
								       tabindex="9"
								       type="text"
								       id="antisp3"
								       name="antisp3"
								       value="<?php
								if ( isset($antisp3) )
								{
									echo htmlspecialchars($antisp3);
								}
								?>"
								/><br />
								<div id="antisp3Error">
								<?php
								if ( isset($antisp3Error) )
								{
									echo "<p class=\"error\">";
									echo $antisp3Error;
									echo "</p>";
								}
								echo "\n";
								?>
								</div>
							</fieldset>
							<input
							       tabindex="10"
							       type="submit"
							       name="registerButton"
							       value="Get an account"
							/>
						</fieldset>
					</form>
				</div>
				<div class="infotext">
					<p>
						Please read the <a href="doc/">documentation</a> before using metodo.
					</p>
				</div>
			</div>

			<div id="bottom">
				<p><?php
				if (isset($bottomText))
				{
					echo $bottomText;
				}
				?></p>
			</div>

		</div>
	</body>
</html>

<!-- EOF //-->
