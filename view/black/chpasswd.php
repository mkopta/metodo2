<?php
if (!defined("INDEX"))
{
	// This file is called in wrong way!
	echo "You probably don't have to be here. Please, go back.";
	die();
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">  
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta http-equiv="content-language" content="en" />
		<meta name="author" content="Martin 'dum8d0g' Kopta http://martin.kopta.eu/" />
		<meta name="keywords" content="metodo2, webtodo, web, todo, poznámky, úkoly, tasks" />
		<meta name="description" content="Web todo organizer" />
		<meta name="robots" content="ALL,FOLLOW" />
		<meta name="cache" content="no-cache" />
		<link rel="stylesheet" type="text/css" href="<?php echo $prefix; ?>chpasswd.css" />
		<link rel="icon" type="image/x-icon" href="<?php echo $prefix; ?>favico.png" />
		<script type="text/javascript" src="<?php echo $prefix; ?>chpasswd.js"></script>
		<title><?php echo htmlspecialchars($pageTitle); ?></title>
	</head>
	<body>
		<div id="content">

			<div id="head">
				<a href="<?php
				echo $_SERVER['PHP_SELF'];
				?>"><img src="<?php echo $prefix; ?>logo.png" alt="Metodo2 logo" /></a>
			</div>

			<div id="middle">
				<div id="form">
					<form action="<?php
					echo $_SERVER['PHP_SELF'];
					?>" method="post" onsubmit="return validate();">
						<fieldset>
							<div id="errors">
							<?php
							if ( isset ( $errorText ) )
							{
								echo "<p class=\"error\">";
								echo $errorText;
								echo "</p>";
							}
							echo "\n";
							?>
							</div>
							<legend>
								Change password for user "<?php echo htmlspecialchars($username); ?>"
							</legend>
							<label for="oldpass">Old password</label>
							<input tabindex="1" type="password" id="oldpass" name="oldpass" value="" /><br />
							<label for="newpass1">New password</label>
							<input tabindex="2" type="password" id="newpass1" name="newpass1" value="" /><br />
							<label for="newpass2">New password</label>
							<input tabindex="3" type="password" id="newpass2" name="newpass2" value="" /><br />

							<input tabindex="4" type="submit" name="chpasswd" value="Change password" />
						</fieldset>
					</form>
				</div>
				<div id="infotext">
					<p>
						<a href="<?php echo $_SERVER['PHP_SELF']; ?>">&lt;&lt; Cancel this action</a>
					</p>
				</div>
			</div>

			<div id="bottom">
				<p><?php if(isset($bottomText)){echo $bottomText;} ?></p>
			</div>

		</div>
	</body>
</html>

