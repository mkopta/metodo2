

function validate()
{
	/* 
	 * Variables
	 */
	var x = true; // return value
	var oldpass  = document.getElementById("oldpass");
	var newpass1 = document.getElementById("newpass1");
	var newpass2 = document.getElementById("newpass2");
	var errplace = document.getElementById("errors");
	var passwdRE = /^.{4,50}$/;

	/*
	 * Reset error colors
	 */
	oldpass.style.backgroundColor  = 'black';
	newpass1.style.backgroundColor = 'black';
	newpass2.style.backgroundColor = 'black';

	/*
	 * Clear old errors
	 */
	if ( errplace.hasChildNodes() )
	{
		while ( errplace.childNodes.length >= 1 )
		{
			errplace.removeChild( errplace.firstChild );       
		} 
	}

	/*
	 * Tests
	 */
	if ( oldpass.value == '' )
	{
		oldpass.style.backgroundColor = '#f66';
		oldpass.focus();
		var atr1     = document.createAttribute("class");
		var atr2     = document.createAttribute("id");
		var err      = document.createElement('p');
		atr1.nodeValue = "error";
		atr2.nodeValue = "errorText";
		err.innerHTML = 'You have to enter your old password!';
		err.setAttributeNode(atr1);
		err.setAttributeNode(atr2);
		errplace.appendChild(err);
		x = false;
	}

	if ( newpass1.value == '' )
	{
		newpass1.style.backgroundColor = '#f66';
		newpass1.focus();
		var atr1     = document.createAttribute("class");
		var atr2     = document.createAttribute("id");
		var err      = document.createElement('p');
		atr1.nodeValue = "error";
		atr2.nodeValue = "errorText";
		err.innerHTML = 'You have to type your new password!';
		err.setAttributeNode(atr1);
		err.setAttributeNode(atr2);
		errplace.appendChild(err);
		x = false;
	}

	if ( newpass2.value == '' )
	{
		newpass2.style.backgroundColor = '#f66';
		newpass2.focus();
		var atr1     = document.createAttribute("class");
		var atr2     = document.createAttribute("id");
		var err      = document.createElement('p');
		atr1.nodeValue = "error";
		atr2.nodeValue = "errorText";
		err.innerHTML = 'You have to type your new password twice!';
		err.setAttributeNode(atr1);
		err.setAttributeNode(atr2);
		errplace.appendChild(err);
		x = false;
	}

	if ( newpass1.value != '' && newpass2.value != '' &&
		 newpass1.value != newpass2.value )
	{
		newpass1.style.backgroundColor = '#f66';
		newpass2.style.backgroundColor = '#f66';
		newpass1.focus();
		newpass1.select();
		var atr1     = document.createAttribute("class");
		var atr2     = document.createAttribute("id");
		var err      = document.createElement('p');
		atr1.nodeValue = "error";
		atr2.nodeValue = "errorText";
		err.innerHTML = 'New passwords does not match!';
		err.setAttributeNode(atr1);
		err.setAttributeNode(atr2);
		errplace.appendChild(err);
		x = false;
	}
	else if ( newpass1.value != '' && newpass2.value != '' &&
	          newpass1.value == newpass2.value )
	{
		if ( passwdRE.test(newpass1.value) == false )
		{
			newpass1.style.backgroundColor = '#f66';
			newpass2.style.backgroundColor = '#f66';
			newpass1.focus();
			newpass1.select();
			var atr1     = document.createAttribute("class");
			var atr2     = document.createAttribute("id");
			var err      = document.createElement('p');
			atr1.nodeValue = "error";
			atr2.nodeValue = "errorText";
			err.innerHTML = 'Password must be from at least 4 characters long (max 50).';
			err.setAttributeNode(atr1);
			err.setAttributeNode(atr2);
			errplace.appendChild(err);
			x = false;
		}
	}


	return x;
}



function pageload()
{
	document.getElementById("oldpass").focus();
}

if (window.addEventListener)
{ window.addEventListener("load", pageload, false); }
else if (window.attachEvent)
{ window.attachEvent("onload", pageload); }

