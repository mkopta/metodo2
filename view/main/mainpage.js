function validate()
{
	/* 
	 * Variables
	 */
	var text        = document.getElementById("todoTextArea");
	var errplace    = document.getElementById("errors");

	/*
	 * Clear old errors
	 */
	if ( errplace.hasChildNodes() )
	{
		while ( errplace.childNodes.length >= 1 )
		{
			errplace.removeChild( errplace.firstChild );       
		} 
	}

	/* 
	 * Tests
	 */
	if ( text.value == '' )
	{
		text.focus();
		var atr1     = document.createAttribute("class");
		var atr2     = document.createAttribute("id");
		var err      = document.createElement('p');
		atr1.nodeValue = "error";
		atr2.nodeValue = "errorText";
		err.innerHTML = 'You forget to write your todo!';
		err.setAttributeNode(atr1);
		err.setAttributeNode(atr2);
		errplace.appendChild(err);
		return false;
	}

	return true;
}

window.onload = function () {
	var headers = getElementsByClassName("todohead");

	for ( i=0; i < headers.length; i++ )
	{
		headers[i].onmouseover = function() { highlightOn(this) };
		headers[i].onmouseout = function() { highlightOff(this) };
	}
};

function highlightOn (el)
{
	el.style.backgroundColor = "#155096"
}

function highlightOff (el)
{
	el.style.backgroundColor = "#3771C8";
}


// EOF
