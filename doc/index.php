<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">  
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta http-equiv="content-language" content="cs" />
		<meta name="author" content="Martin 'dum8d0g' Kopta http://martin.kopta.eu/" />
		<meta name="keywords" content="metodo2, webtodo, web, todo, poznámky, úkoly, tasks, dokumentace, návod" />
		<meta name="description" content="Web todo organizer" />
		<meta name="robots" content="ALL,FOLLOW" />
		<meta name="cache" content="no-cache" />
		<link rel="stylesheet" type="text/css" href="design/doc.css" />
		<link rel="icon" type="image/x-icon" href="design/favico.png" />
		<title>Metodo2 - dokumentace</title>
	</head>
	<body>
		<div id="content">

			<div id="left">

				<div id="head">
					<a href="index.php"><img src="design/mediumlogo.png" alt="Metodo2 logo" /></a>
				</div>

				<div id="menu">

					<ol>
						<li>
							Popis úlohy
							<ol>
								<li><a href="index.php?page=zadani">Zadání</a></li>
								<li><a href="index.php?page=pozadavky">Funkční požadavky</a></li>
								<li><a href="index.php?page=role">Uživatelské role</a></li>
							</ol>
						</li>
						<li>
							Uživatelská příručka
							<ol>
								<li><a href="index.php?page=funkcnost">Funkčnost</a></li>
								<li><a href="index.php?page=instalace">Instalace</a></li>
								<li><a href="index.php?page=ui">Uživatelské rozhraní</a></li>
							</ol>
						</li>
						<li>
							Popis implementace
							<ol>
								<li><a href="index.php?page=rysy">Hlavní rysy</a></li>
								<li><a href="index.php?page=form">Obsluha formulářů</a></li>
								<li><a href="index.php?page=db">Databáze</a></li>
							</ol>
						</li>
						<li><a href="index.php?page=faq">FAQ</a></li>
						<li><a href="index.php?page=author">Author</a></li>
					</ol>
					<br />
					<a href="../">Zpět k aplikaci</a>
				</div>

				<div id="bottom">
					<p>(c) Martin Kopta, 2008</p>
				</div>

			</div>

			<div id="right">
				<?php
				if (!isset($_GET['page']))
						include('content/faq.html');
				else
					{
						switch ($_GET['page'])
						{
							case 'zadani':
								include('content/zadani.html');
								break;
							case 'pozadavky':
								include('content/pozadavky.html');
								break;
							case 'role':
								include('content/role.html');
								break;
							case 'funkcnost':
								include('content/funkcnost.html');
								break;
							case 'instalace':
								include('content/instalace.html');
								break;
							case 'ui':
								include('content/ui.html');
								break;
							case 'rysy':
								include('content/rysy.html');
								break;
							case 'form':
								include('content/form.html');
								break;
							case 'db':
								include('content/db.html');
								break;
							case 'faq':
								include('content/faq.html');
								break;
							case 'author':
								include('content/author.html');
								break;
							default:
								include('content/faq.html');
						}
					}
				?>
			</div>

			<div class="cleaner" />

		</div>
	</body>
</html>
