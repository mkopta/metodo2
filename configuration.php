<?php
if (!defined("INDEX"))
{
	// This file is called in wrong way!
	echo "You probably don't have to be here. Please, go back.";
	die();
}

/* Timezone */
date_default_timezone_set("Europe/Prague");

/* Database connection variables */
define("DB_SERVER",   "localhost");
define("DB_DATABASE", "metodo2");
define("DB_USERNAME", "metodo2");
define("DB_PASSWORD", "metodo2");
//define("DB_PASSWORD", "badpasswd");

/* Presentation style
   Styles are in directory APP_STYLEDIR, each style has own directory,
   default style is main, when is style not defined, app will crash.
*/
define("APP_STYLEDIR","view");
define("APP_STYLE",   "main");


/* Cookie
   Defines cookie name
*/
define("COOKIE_NAME", "metodo2");

?>
