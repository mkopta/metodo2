<?php

if (!defined("INDEX"))
{
	// This file is called in wrong way!
	echo "You probably don't have to be here. Please, go back.";
	die();
}

class cDatabase
{

	private $db;
	public  $isError;
	
	public function __construct($server, $username, $password, $database)
	{
		$this->isError = false;
		try
		{
			$db = @new mysqli($server, $username, $password, $database);
			if( mysqli_connect_errno() )
			{
				throw new Exception ('connection to database failed!');
			}
			$db->set_charset('utf8');
			$this->db = $db;
		}
		catch (Exception $e)
		{
			$this->isError = true;
		}
	}

	public function __destruct()
	{
		if(!$this->isError)
		{
			$this->db->close();
		}
	}

	private function todayDate()
	{
		return date ( "Y-m-d" );
	}

	private function safestring($query)
	{
		if ( get_magic_quotes_gpc() )
		{
			$query = stripslashes($query);
		}
		return $this->db->real_escape_string($query);
	}

	public function checkAuth($username, $passhash)
	{
		$username = $this->safestring($username);

		$query = "SELECT passhash FROM Users WHERE username='".$username."'";
		if ($result = $this->db->query($query))
		{
			$row = $result->fetch_object();
			if ( isset($row) and $row->passhash == $passhash )
			{
				$result->close();
				return true;
			}
		}
		return false;
	}

	public function createUser($username, $passhash, $firstname, $lastname, $contact)
	{
		$username  = $this->safestring($username);
		$firstname = $this->safestring($firstname);
		$lastname  = $this->safestring($lastname);
		$contact   = $this->safestring($contact);

		$query  = "INSERT INTO Users SET ";
		$query .= "idusr = '',";
		$query .= "username = '$username',";
		$query .= "passhash = '$passhash',";
		$query .= "firstname = '$firstname',";
		$query .= "lastname = '$lastname',";
		$query .= "contact = '$contact'";

		return $this->db->query($query);
	}

	public function userExists($username)
	{
		$username  = $this->safestring($username);

		$query = "SELECT username FROM Users WHERE username='".$username."'";
		if ($result = $this->db->query($query))
		{
			$row = $result->fetch_object();
			if ( isset($row) and $row->username == $username ){ return true; }
		}
		return false;
	}

	public function addTodo($author, $priority, $deadline, $text, $labels)
	{
		$author   = $this->safestring($author);
		$text     = $this->safestring($text);
		// securing $priority
		switch ($priority)
		{
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
				break;
			default:
				$priority = 3; // better bad value, than crash
		}


		if ( ! isset ( $deadline ) ) { $deadline = "NULL"; }
		else { $deadline = "'".$this->safestring($deadline)."'"; }

		$enteredDate = $this->todayDate();
		$query  = "START TRANSACTION;\n";
		$query .= "SELECT @IDT:=MAX(idtodo)+1 FROM Todos;\n";
		$query .= "SELECT @IDU:=idusr FROM Users WHERE username=\"".$author."\";\n";
		$query .= "INSERT INTO Todos VALUES (";
		$query .= "'0', @IDU, @IDT, ".$priority.", '".$enteredDate."', ";
		$query .= "".$deadline.", '".$text."');\n";
		if ( ! empty($labels) )
		{
			foreach ( $labels as $label )
			{
				if ( empty ( $label ) ) continue;
				$label = $this->safestring($label);
				$query .= "INSERT IGNORE INTO Labels VALUES ('', '".$label."');\n";
			}

			foreach ( $labels as $label )
			{
				if ( empty ( $label ) ) continue;
				$label = $this->safestring($label);
				$query .= "INSERT INTO Labeled VALUES ";
				$query .= "(@IDT, (SELECT idlabel FROM Labels WHERE text=\"".$label."\"));\n";
			}
		}
		$query .= "COMMIT";

		if ( $this->db->multi_query($query) )
		{
			do
			{
				if ($result = $this->db->store_result())
				{
					$result->free();
				}
			}
			while ($this->db->next_result());
			return true;
		}
		return false;
	}

	public function addDoneTodo($author, $priority, $deadline, $text, $labels)
	{
		$author   = $this->safestring($author);
		$text     = $this->safestring($text);
		// securing $priority
		switch ($priority)
		{
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
				break;
			default:
				$priority = 3; // better bad value, than crash
		}

		if ( ! isset ( $deadline ) ) { $deadline = "NULL"; }
		else { $deadline = "'".$this->safestring($deadline)."'"; }

		$enteredDate = $this->todayDate();
		$query  = "START TRANSACTION;\n";
		$query .= "SELECT @IDT:=MAX(idtodo)+1 FROM Todos;\n";
		$query .= "SELECT @IDU:=idusr FROM Users WHERE username=\"".$author."\";\n";
		$query .= "INSERT INTO Todos VALUES (";
		$query .= "'1', @IDU, @IDT, ".$priority.", '".$enteredDate."', ";
		$query .= "".$deadline.", '".$text."');\n";
		if ( ! empty($labels) )
		{
			foreach ( $labels as $label )
			{
				if ( empty ( $label ) ) continue;
				$label = $this->safestring($label);
				$query .= "INSERT IGNORE INTO Labels VALUES ('', '".$label."');\n";
			}

			foreach ( $labels as $label )
			{
				if ( empty ( $label ) ) continue;
				$label = $this->safestring($label);
				$query .= "INSERT INTO Labeled VALUES ";
				$query .= "(@IDT, (SELECT idlabel FROM Labels WHERE text=\"".$label."\"));\n";
			}
		}
		$query .= "COMMIT";

		if ( $this->db->multi_query($query) )
		{
			do
			{
				if ($result = $this->db->store_result())
				{
					$result->free();
				}
			}
			while ($this->db->next_result());
			return true;
		}
		return false;
	}

	public function getPendingTodosSum($username)
	{
		$username = $this->safestring($username);

		$query  = "SELECT COUNT(idtodo) as x ";
		$query .= "FROM Todos t JOIN Users u ON t.u_idusr=u.idusr ";
		$query .= "WHERE t.t_done='0' AND u.username='".$username."';";
		if ($result = $this->db->query($query))
		{
			$row = $result->fetch_object();
			if ( isset($row) ) { return $row->x; }
			return null;
		}
		return null;
	}

	public function getAllTodos($username)
	{
		$username = $this->safestring($username);

		$i = 0;
		$lastID = -1;
		$todos  = null;
		$query  = "SELECT ";
		$query .= "  t_done, idtodo, priority, enteredDATE, ";
		$query .= "  deadline, t.text, l.text AS label ";
		$query .= "FROM Todos t ";
		$query .= "LEFT JOIN ( ";
		$query .= "       SELECT i_idtodo, text ";
		$query .= "       FROM Labeled JOIN Labels ON l_idlabel = idlabel ";
		$query .= ") l ON l.i_idtodo=t.idtodo ";
		$query .= "WHERE u_idusr = ( ";
		$query .= "                  SELECT idusr ";
		$query .= "                  FROM Users ";
		$query .= "                  WHERE username=\"".$username."\"";
		$query .= "                ) ";
		$query .= "ORDER BY idtodo DESC";

		if ($result = $this->db->query($query))
		{
			while ($row = mysqli_fetch_object($result)) {
				if ( $row->idtodo != $lastID )
				{
					$i++;
					$lastID = $row->idtodo;
					$todos[$i]['idtodo'] = $row->idtodo;
					$todos[$i]['t_done'] = $row->t_done;
					$todos[$i]['priority'] = $row->priority;
					$todos[$i]['enteredDATE'] = $row->enteredDATE;
					$todos[$i]['deadline'] = $row->deadline;
					$todos[$i]['text'] = $row->text;
					$todos[$i]['labels'] = $row->label;
				}
				else
				{
					$todos[$i]['labels'] .= ", ".$row->label;
				}
			}
			mysqli_free_result($result);
			return $todos;
		}
		return false;
	}

	public function setPass($username, $passhash)
	{
		$username = $this->safestring($username);

		$query  = "UPDATE Users SET ";
		$query .= "passhash = '".$passhash."'";
		$query .= "WHERE username = '".$username."'";
		return $this->db->query($query);
	}


	/* Sets todo with ID $idtodo which belongs to $username
	 * to state $state. For state $state=1 it sets todo with 
	 * $idtodo as done, for state $state!=1 sets todo with
	 * $idtodo as undone.
	 */
	public function setTodoState($username, $idtodo, $state)
	{
		$username = $this->safestring($username);
		$idtoto = $this->safestring($idtoto);

		if ( $state == 1 )
		{
			$set = 1;
		}
		else
		{
			$set = 0;
		}
		$query  = "UPDATE Todos ";
		$query .= "SET t_done = ".$set." ";
		$query .= "WHERE idtodo = '";
		$query .= $idtodo."' ";
		$query .= "AND u_idusr = ( ";
		$query .= " SELECT idusr ";
		$query .= "	FROM Users ";
		$query .= "	WHERE username='";
		$query .= $username;
		$query .= "' )";
		return $this->db->query($query);
	}

	/* Delete one Todo entry, all idtodo relevant entries 
	 * in Labeled and all (from now on) unused labels
	 */
	public function removeTodo($username, $idtodo)
	{
		$username = $this->safestring($username);
		$idtodo = $this->safestring($idtodo);

		$query  = "BEGIN;\n";
		$query .= "DELETE FROM Todos WHERE idtodo=".$idtodo.";\n";
		$query .= "DELETE FROM Labeled WHERE i_idtodo=".$idtodo.";\n";
		$query .= "DELETE FROM Labels WHERE idlabel NOT IN (SELECT l_idlabel FROM Labeled);\n";
		/*$query .= "DELETE FROM Labels ";
		$query .= "  LEFT JOIN Labeled ON l_idlabel=idlabel ";
		$query .= "  WHERE i_idtodo IS NULL;\n";*/ // What is faster?
		$query .= "COMMIT;\n";
		if ( $this->db->multi_query($query) )
		{
			do
			{
				if ($result = $this->db->store_result())
				{
					$result->free();
				}
			}
			while ($this->db->next_result());
			return true;
		}
		return false;
	}


	public function getAllTodosWithPriority($username, $priority)
	{
		$username = $this->safestring($username);
		$priority = $this->safestring($priority);

		$i = 0;
		$lastID = -1;
		$todos  = null;
		$query  = "SELECT ";
		$query .= "  t_done, idtodo, priority, enteredDATE, ";
		$query .= "  deadline, t.text, l.text AS label ";
		$query .= "FROM Todos t ";
		$query .= "LEFT JOIN ( ";
		$query .= "       SELECT i_idtodo, text ";
		$query .= "       FROM Labeled JOIN Labels ON l_idlabel = idlabel ";
		$query .= ") l ON l.i_idtodo=t.idtodo ";
		$query .= "WHERE u_idusr = ( ";
		$query .= "                  SELECT idusr ";
		$query .= "                  FROM Users ";
		$query .= "                  WHERE username=\"".$username."\"";
		$query .= "                ) ";
		$query .= "AND priority=".$priority."; ";

		if ($result = $this->db->query($query))
		{
			while ($row = mysqli_fetch_object($result)) {
				if ( $row->idtodo != $lastID )
				{
					$i++;
					$lastID = $row->idtodo;
					$todos[$i]['idtodo'] = $row->idtodo;
					$todos[$i]['t_done'] = $row->t_done;
					$todos[$i]['priority'] = $row->priority;
					$todos[$i]['enteredDATE'] = $row->enteredDATE;
					$todos[$i]['deadline'] = $row->deadline;
					$todos[$i]['text'] = $row->text;
					$todos[$i]['labels'] = $row->label;
				}
				else
				{
					$todos[$i]['labels'] .= ", ".$row->label;
				}
			}
			mysqli_free_result($result);
			return $todos;
		}
		return false;
	}

	public function getAllLabels($username)
	{
		$username = $this->safestring($username);

		$i = 0;
		$labels = null;
		$query  = "SELECT \n";
		$query .= "l.idlabel AS id, l.text AS text\n";
		$query .= "FROM Todos t \n";
		$query .= "LEFT JOIN ( \n";
		$query .= "       SELECT i_idtodo, text, idlabel\n";
		$query .= "       FROM Labeled JOIN Labels ON l_idlabel = idlabel \n";
		$query .= ") l ON l.i_idtodo=t.idtodo \n";
		$query .= "WHERE u_idusr = ( \n";
		$query .= "                  SELECT idusr \n";
		$query .= "                  FROM Users \n";
		$query .= "                  WHERE username='".$username."'\n";
		$query .= "                )\n";
		$query .= "AND NOT l.text IS NULL\n";
		$query .= "GROUP BY l.idlabel\n";

		if ($result = $this->db->query($query))
		{
			while ($row = mysqli_fetch_object($result))
			{
					$i = $row->id;
					$labels[$i] = $row->text;
			}
			mysqli_free_result($result);
			return $labels;
		}
		return false;
	}

	public function getAllTodosDone($username, $done)
	{
		$username = $this->safestring($username);
		if ( $done != 1 ) { $done = 0; } // only 0 and 1 allowed
		$i = 0;
		$lastID = -1;
		$todos  = null;
		$query  = "SELECT ";
		$query .= "  t_done, idtodo, priority, enteredDATE, ";
		$query .= "  deadline, t.text, l.text AS label ";
		$query .= "FROM Todos t ";
		$query .= "LEFT JOIN ( ";
		$query .= "       SELECT i_idtodo, text ";
		$query .= "       FROM Labeled JOIN Labels ON l_idlabel = idlabel ";
		$query .= ") l ON l.i_idtodo=t.idtodo ";
		$query .= "WHERE u_idusr = ( ";
		$query .= "                  SELECT idusr ";
		$query .= "                  FROM Users ";
		$query .= "                  WHERE username=\"".$username."\"";
		$query .= "                ) ";
		$query .= "AND t_done=\"".$done."\"; ";

		if ($result = $this->db->query($query))
		{
			while ($row = mysqli_fetch_object($result)) {
				if ( $row->idtodo != $lastID )
				{
					$i++;
					$lastID = $row->idtodo;
					$todos[$i]['idtodo'] = $row->idtodo;
					$todos[$i]['t_done'] = $row->t_done;
					$todos[$i]['priority'] = $row->priority;
					$todos[$i]['enteredDATE'] = $row->enteredDATE;
					$todos[$i]['deadline'] = $row->deadline;
					$todos[$i]['text'] = $row->text;
					$todos[$i]['labels'] = $row->label;
				}
				else
				{
					$todos[$i]['labels'] .= ", ".$row->label;
				}
			}
			mysqli_free_result($result);
			return $todos;
		}
		return false;
	}


	public function restoreDemoAccount()
	{
		$query  = "BEGIN; \n";
		// Remove 'demo' account 
		$query .= "DELETE FROM Users WHERE username='demo'; \n";
		// Remove todo entries of demo user
		$query .= "DELETE FROM Todos WHERE u_idusr NOT IN (SELECT idusr FROM Users); \n";
		// Remove all nonvalid entries from Labeld
		$query .= "DELETE FROM Labeled WHERE i_idtodo NOT IN (SELECT idtodo FROM Todos); \n";
		// Remove all unused labels from Labels
		$query .= "DELETE FROM Labels WHERE idlabel NOT IN (SELECT l_idlabel FROM Labeled); \n";
		$query .= "COMMIT; \n";

		if ( $this->db->multi_query($query) )
		{
			do // just clear socket
			{ if ($result = $this->db->store_result()) { $result->free(); } }
			while ($this->db->next_result());
		}
		else
		{ return false; }

		// Create account 'demo'
		$rv = $this->createUser(
			'demo',
			'89e495e7941cf9e40e6980d14a16bf023ccd4c91', //demo
			null,
			null,
			null
		);
		if ( ! $rv ) { return false; }


		// Add data to 'demo' account
		$rv = $this->addTodo('demo', '5', null,
			'Toto je nehotové todo nejvyšší priority',
			array('demo label 4'));
		if ( ! $rv ) { return false; }

		$rv = $this->addTodo('demo', '4', null,
			'Toto je nehotové todo vysoké priority',
			array('demo label 1', 'demo label 2'));
		if ( ! $rv ) { return false; }

		$rv = $this->addTodo('demo', '3', null,
			'Toto je nehotové todo střední priority',
			array('demo label 2'));
		if ( ! $rv ) { return false; }

		$rv = $this->addTodo('demo', '2', null,
			'Toto je nehotové todo nízké priority',
			array('demo label 2', 'demo label 3'));
		if ( ! $rv ) { return false; }

		$rv = $this->addTodo('demo', '1', null,
			'Toto je nehotové todo nejnižší priority',
			array('demo label 1'));
		if ( ! $rv ) { return false; }

		$rv = $this->addDoneTodo('demo', '5', null,
			'Toto je hotové todo nejvyšší priority',
			array('demo label 3', 'demo label 1'));
		if ( ! $rv ) { return false; }

		$rv = $this->addDoneTodo('demo', '4', null, 
			'Toto je hotové todo vysoké priority',
			array('demo label 3'));
		if ( ! $rv ) { return false; }

		$rv = $this->addDoneTodo('demo', '3', null,
			'Toto je hotové todo střední priority',
			array('demo label 3', 'demo label 4'));
		if ( ! $rv ) { return false; }

		$rv = $this->addDoneTodo('demo', '2', null,
			'Toto je hotové todo nízké priority',
			array('demo label 1', 'demo label 2'));
		if ( ! $rv ) { return false; }

		$rv = $this->addDoneTodo('demo', '1', null, 
			'Todo je hotové todo nejnižší priority',
			array('demo label 3'));
		if ( ! $rv ) { return false; }

		$rv = $this->addTodo('demo', '4', '2008-11-30',
			'Toto je todo s prošlým termínem',
			array('demo label 1', 'demo label 3'));
		if ( ! $rv ) { return false; }

		$rv = $this->addTodo('demo', '2', '2018-12-30',
			'Toto je todo s termínem',
			array('demo label 2'));
		if ( ! $rv ) { return false; }

		return true;
	}

	public function todoExists($username, $idtodo)
	{
		$username = $this->safestring($username);
		$idtodo = $this->safestring($idtodo);

		$query  = "SELECT t.idtodo \n";
		$query .= "FROM Todos t \n";
		$query .= "WHERE t.idtodo=".$idtodo." \n";
		$query .= "AND t.u_idusr=( SELECT idusr \n";
		$query .= "                FROM Users \n";
		$query .= "                WHERE username='".$username."' \n";
		$query .= "              ) \n";

		if ($result = $this->db->query($query))
		{
			$row = $result->fetch_object();
			if ( isset($row) )
			{
				return true;
			}
			return false;
		}
		return false;
	}

	public function getTodoLabels($idtodo)
	{
		$idtodo = $this->safestring($idtodo);

		$i = 0;
		$labels = null;
		$query  = "SELECT text \n";
		$query .= "FROM Labels lb \n";
		$query .= "JOIN Labeled ld \n";
		$query .= "ON lb.idlabel = ld.l_idlabel \n";
		$query .= "WHERE i_idtodo = ".$idtodo." \n";

		if ($result = $this->db->query($query))
		{
			while ($row = mysqli_fetch_object($result))
			{
					$labels[$i] = $row->text;
					$i++;
			}
			mysqli_free_result($result);
			return $labels;
		}
		return false;
	}

	public function getTodoPriority($idtodo)
	{
		$idtodo = $this->safestring($idtodo);

		$query  = "SELECT t.priority \n";
		$query .= "FROM Todos t \n";
		$query .= "WHERE t.idtodo=".$idtodo." \n";

		if ($result = $this->db->query($query))
		{
			$row = $result->fetch_object();
			if ( isset($row) )
			{
				return $row->priority;
			}
			return "";
		}
		return "";
	}

	public function getTodoDeadline($idtodo)
	{
		$idtodo = $this->safestring($idtodo);

		$query  = "SELECT t.deadline \n";
		$query .= "FROM Todos t \n";
		$query .= "WHERE t.idtodo=".$idtodo." \n";

		if ($result = $this->db->query($query))
		{
			$row = $result->fetch_object();
			if ( isset($row) )
			{
				return $row->deadline;
			}
			return "";
		}
		return "";
	}

	public function getTodoText($idtodo)
	{
		$idtodo = $this->safestring($idtodo);

		$query  = "SELECT t.text \n";
		$query .= "FROM Todos t \n";
		$query .= "WHERE t.idtodo=".$idtodo." \n";

		if ($result = $this->db->query($query))
		{
			$row = $result->fetch_object();
			if ( isset($row) )
			{
				return $row->text;
			}
			return "";
		}
		return "";
	}


	public function getAllTodosWithLabel($username, $idlabel)
	{
		$username = $this->safestring($username);
		$idlabel = $this->safestring($idlabel);

		$i = 0;
		$lastID = -1;
		$todos  = null;
		$query  = "SELECT \n";
		$query .= "  t_done, idtodo, priority, enteredDATE,  \n";
		$query .= "  deadline, t.text, l.text AS label  \n";
		$query .= "FROM Todos t  \n";
		$query .= "LEFT JOIN (  \n";
		$query .= "       SELECT i_idtodo, text \n";
		$query .= "       FROM Labeled JOIN Labels ON l_idlabel = idlabel  \n";
		$query .= ") l ON l.i_idtodo=t.idtodo  \n";
		$query .= "WHERE u_idusr = (  \n";
		$query .= "                  SELECT idusr  \n";
		$query .= "                  FROM Users  \n";
		$query .= "                  WHERE username='".$username."' \n";
		$query .= "                )  \n";
		$query .= "  AND idtodo IN ( \n";
		$query .= "                  SELECT i_idtodo \n";
		$query .= "                  FROM Labeled \n";
        $query .= "                  WHERE l_idlabel='".$idlabel."' \n";
		$query .= "                ); ";

		if ($result = $this->db->query($query))
		{
			while ($row = mysqli_fetch_object($result)) {
				if ( $row->idtodo != $lastID )
				{
					$i++;
					$lastID = $row->idtodo;
					$todos[$i]['idtodo'] = $row->idtodo;
					$todos[$i]['t_done'] = $row->t_done;
					$todos[$i]['priority'] = $row->priority;
					$todos[$i]['enteredDATE'] = $row->enteredDATE;
					$todos[$i]['deadline'] = $row->deadline;
					$todos[$i]['text'] = $row->text;
					$todos[$i]['labels'] = $row->label;
				}
				else
				{
					$todos[$i]['labels'] .= ", ".$row->label;
				}
			}
			mysqli_free_result($result);
			return $todos;
		}
		return false;
	}

}

/* EOF */ ?>
