<?php

class cView
{

	private $data;

	public function __construct()
	{
		$this->data = array();
	}

	public function set($key, $value)
	{
		$this->data[$key] = $value;
	}

	public function show($template)
	{
		if (!empty($this->data))
			extract($this->data);
		require($prefix.$template);
	}

}

?>
