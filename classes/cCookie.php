<?php

if (!defined("INDEX"))
{
	// This file is called in wrong way!
	echo "You probably don't have to be here. Please, go back.";
	die();
}

class cCookie
{
	private $username;
	private $passhash;
	private $filter_by;
	private $filter_id;
	private $filter_lv;
	private $validity;
	private $isGiven;
	private $cookieName;

	public function __construct($cookieName)
	{
		$this->username   = null;		
		$this->passhash   = null;		
		$this->validity   = null;
		$this->filter_by  = null;
		$this->filter_id  = null;
		$this->filter_lv  = null;
		$this->cookieName = $cookieName;

		if ( isset ( $_COOKIE[$this->cookieName] ) )
		{
			$this->isGiven = true;
		}
		else
		{
			$this->isGiven = false;
		}
	}

	public function isGiven()
	{
		return $this->isGiven;
	}

	public function read()
	{
		/* Stucture of Cookie:
		   username = username
		   passhash = password hash
		 */
		foreach ($_COOKIE[$this->cookieName] as $name => $value)
		{
			// Jedine povolene promene v susenkach..
			// kdyby jich bylo vic tak si udelam asi pole
			// ale tak zatim susenky slouzi jenom na heslo a jmeno..
			if ( $name == "username" )
			{
				$this->username = $value;
			}
			else if ( $name == "passhash" )
			{
				$this->passhash = $value;
			}
			else if ( $name == "filter_by" ) 
			{
				$this->filter_by = $value;
			}
			else if ( $name == "filter_id" )
			{
				$this->filter_id = $value;
			}
			else if ( $name == "filter_lv" )
			{
				$this->filter_lv = $value;
			}
			else
			{
				$this->validity = false;
				return;
			}
		}
		$this->validity = true;
		return;
	}

	public function saveCookie()
	{
		setCookie($this->cookieName."[username]", $this->username);
		setCookie($this->cookieName."[passhash]", $this->passhash);
		setCookie($this->cookieName."[filter_by]", $this->filter_by);
		setCookie($this->cookieName."[filter_id]", $this->filter_id);
		setCookie($this->cookieName."[filter_lv]", $this->filter_lv);
	}

	public function deleteCookie()
	{
		foreach ($_COOKIE[$this->cookieName] as $name => $value)
			setcookie ($this->cookieName."[".$name."]", "", time() - 3600);
	}

	public function clearfilters()
	{
		$this->filter_by = null;
		$this->filter_id = null;
		$this->filter_lv = null;
	}

	public function isValid()     { return $this->validity; }
	public function getUsername() { return $this->username; }
	public function getPasshash() { return $this->passhash; }
	public function getFilterBy() { return $this->filter_by; }
	public function getFilterId() { return $this->filter_id; }
	public function getFilterLv() { return $this->filter_lv; }

	public function setUsername($username) { $this->username = $username; }
	public function setPasshash($passhash) { $this->passhash = $passhash; }
	public function setFilterBy($val) { $this->filter_by = $val; }
	public function setFilterId($val) { $this->filter_id = $val; }
	public function setFilterLv($val) { $this->filter_lv = $val; }

}

?>
