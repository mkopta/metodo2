<?php

if (!defined("INDEX"))
{
	// This file is called in wrong way!
	echo "You probably don't have to be here. Please, go back.";
	die();
}


class ccCookieLover extends ccSkeleton
{
	private $myObjects;
	private $cookie_name;
	private $app_styledir;
	private $app_style;
	private $db_server;
	private $db_database;
	private $db_username;
	private $db_password;

	public function __construct($cookie_name, $app_styledir, $app_style,
	                            $db_server, $db_database, $db_username,
	                            $db_password)
	{
		$this->myObjects  = array();
		$this->cookie_name  = $cookie_name;
		$this->app_styledir = $app_styledir;
		$this->app_style    = $app_style;
		$this->db_server    = $db_server;
		$this->db_database  = $db_database;
		$this->db_username  = $db_username;
		$this->db_password  = $db_password;
	}

	private function showMainPage()
	{
		$filterby = $this->myObjects['Cookie']->getFilterBy();
		if ( $filterby != null && !empty($filterby) ) 
		{
			$this->showFilteredMainPage();
			return 0;
		}
		
		$O = $this->myObjects;
		$O['View']->set("prefix", $this->app_styledir."/".$this->app_style."/");
		$O['View']->set("pageTitle", "Metodo 2 - web todo");
		$O['View']->set("bottomText", "&copy; Martin Kopta, 2008");
		$O['View']->set(
			"pendingTodosSum",
			$O['DB']->getPendingTodosSum
			(
				$O['Cookie']->getUsername()
			)
		);
		$O['View']->set
		(
			"listOfTodos",
			$O['DB']->getAllTodos
			(
				$O['Cookie']->getUsername()
			)
		);
		$O['View']->set
		(
			"labels",
			$O['DB']->getAllLabels
			(
				$O['Cookie']->getUsername()
			)
		);
		$O['View']->set("username", $O['Cookie']->getUsername());
		$O['View']->show('mainpage.php');

		return 0;
	}

	private function showFilteredMainPage()
	{
		$filterby = $this->myObjects['Cookie']->getFilterBy();
		switch ( $filterby )
		{
			case 'priority':
				$lv = $this->myObjects['Cookie']->getFilterLv();
				if ( $lv == null || empty($lv) ) {break;}
				$this->myObjects['View']->set
					(
					 "listOfTodos",
					 $this->myObjects['DB']->getAllTodosWithPriority
					 (
					  $this->myObjects['Cookie']->getUsername(),
					  $lv
					 )
					);
				break;

			case 'undone':
				$this->myObjects['View']->set
					(
					 "listOfTodos",
					 $this->myObjects['DB']->getAllTodosDone
					 (
					  $this->myObjects['Cookie']->getUsername(),
					  0
					 )
					);
				break;

			case 'done':
				$this->myObjects['View']->set
					(
					 "listOfTodos",
					 $this->myObjects['DB']->getAllTodosDone
					 (
					  $this->myObjects['Cookie']->getUsername(),
					  1
					 )
					);
				break;

			case 'label':
				$id = $this->myObjects['Cookie']->getFilterId();
				if ( $id == null || empty($id) ) {break;}
				$this->myObjects['View']->set
					(
					 "listOfTodos",
					 $this->myObjects['DB']->getAllTodosWithLabel
					 (
					  $this->myObjects['Cookie']->getUsername(),
					  $id
					 )
					);
				break;

			default:
				// unknown filter, select all todos (something bad happend)
				$this->myObjects['View']->set
					(
					 "listOfTodos",
					 $this->myObjects['DB']->getAllTodos
					 (
					  $this->myOjects['Cookie']->getUsername()
					 )
					);
				break;

		}
		$O = $this->myObjects;
		$O['View']->set("prefix", $this->app_styledir."/".$this->app_style."/");
		$O['View']->set("pageTitle", "Metodo 2 - web todo");
		$O['View']->set("bottomText", "&copy; Martin Kopta, 2008");
		$O['View']->set(
				"pendingTodosSum",
				$O['DB']->getPendingTodosSum
				(
				 $O['Cookie']->getUsername()
				)
				);
		$O['View']->set
			(
			 "labels",
			 $O['DB']->getAllLabels
			 (
			  $O['Cookie']->getUsername()
			 )
			);
		$O['View']->set("username", $O['Cookie']->getUsername());
		$O['View']->show('mainpage.php');

		return 0;
	}

	public function start()
	{
		// Vytvoreni objektu pracujicim se susenkami
		$this->myObjects['Cookie'] = new cCookie($this->cookie_name);
		// Vytvoreni objektu zprostredkovavajim zobrazovani
		$this->myObjects['View']   = new cView();

		/*
		   Zde predpokladame, ze jsme susenku od uzivatele dostali a tak
		   ji pekne spapame a zjistime, jestli je to takova susenka, ktera
		   se nam libi nebo jestli je to susenka oskliva. Pokud je oskliva
		   tak ji zahodime a restartujeme controller, pokud je dobra tak
		   muzeme pokracovat dal.
		 */
		$this->myObjects['Cookie']->read();
		if ( ! $this->myObjects['Cookie']->isValid() )
		{
			$this->myObjects['Cookie']->deleteCookie();
			// Refresh !!
			header("location: ".$_SERVER['PHP_SELF']);
			return 0;
		}

		// Vytvoreni objektu pracujicim s databazi
		$this->myObjects['DB'] = new cDatabase
		(
			$this->db_server,
			$this->db_username,
			$this->db_password,
			$this->db_database
		); 
		if ( $this->myObjects['DB']->isError )
		{
			echo "<h1>Dabase error!</h1>\n";
			echo "<p>Connection to database failed, please check the \n";
			echo "configuration in file 'configuration.php'.\n</p></p>In ";
			echo "case of trouble, read the <a href=\"doc/\">";
			echo "documentation</a>.</p><p>If you lost any input or data, ";
			echo "we are terribly sorry, please don't get mad at us.</p>";
			return 0;
		}

		/*
		   Zkontrolujeme jestli susenka od uzivatele je vazne tak dobra
		   a pokud ano, pustime ho dovnitr aplikace ke vsem tem skvelym
		   featuram. Pokud ne, tak susenku zahodime a vratime loginpage
		 */

		if ( ! 
			$this->myObjects['DB']->checkAuth
			(
				$this->myObjects['Cookie']->getUsername(),
				$this->myObjects['Cookie']->getPasshash()
			)
		   )
		{
			$this->myObjects['Cookie']->deleteCookie();

			$this->myObjects['View']->set
			(
				"prefix",
				$this->app_styledir."/".$this->app_style."/"
			);
			$this->myObjects['View']->set("pageTitle", "Metodo 2 - web todo");
			$this->myObjects['View']->set
			(
				"bottomText",
				"&copy; Martin Kopta, 2008"
			);
			$this->myObjects['View']->set
			(
				"errorText",
				"Wrong username or password."
			);
			$this->myObjects['View']->set
			(
				"username",
				$this->myObjects['Cookie']->getUsername()
			);
			$this->myObjects['View']->show('loginpage.php');

			return 0;
		}

		/* ********** */
		/*  MAINPAGE  */
		/* ********** */

		if ( isset($_GET['do']) )
		{
			switch ( $_GET['do'] )
			{
				case 'logout':
					if ( $this->myObjects['Cookie']->getUsername() == "demo" )
					{
						$rv = $this->myObjects['DB']->restoreDemoAccount();
						if ( ! $rv )
						{
							echo "SHIT";
						}
					}
					$this->myObjects['Cookie']->deleteCookie();
					header("location: ".$_SERVER['PHP_SELF']);
					return 0;

				case 'passwd':
					$this->myObjects['View']->set
					(
						"prefix",
						$this->app_styledir."/".$this->app_style."/"
					);
					$this->myObjects['View']->set
						(
						 "pageTitle", "Metodo 2 - web todo");
					$this->myObjects['View']->set
					(
						"bottomText",
						"&copy; Martin Kopta, 2008"
					);
					$this->myObjects['View']->set
					(
						"username",
						$this->myObjects['Cookie']->getUsername()
					);
					$this->myObjects['View']->show('chpasswd.php');
					return 0;

				case 'markundone':
					if ( ! isset($_GET['idtodo']) ) break;

					$this->myObjects['DB']->setTodoState
					(
						$this->myObjects['Cookie']->getUsername(),
						$_GET['idtodo'],
						0
					);
					header("Location: ".$_SERVER['PHP_SELF']);
					return 0;

				case 'markdone':
					if ( ! isset($_GET['idtodo']) ){ break; }
					$this->myObjects['DB']->setTodoState
					(
						$this->myObjects['Cookie']->getUsername(),
						$_GET['idtodo'],
						1
					);
					header("Location: ".$_SERVER['PHP_SELF']);
					return 0;

				case 'remove':
					if ( ! isset ( $_GET [ 'idtodo' ] ) ) { break ; }
					$this->myObjects['DB']->removeTodo
					(
						$this->myObjects['Cookie']->getUsername(),
						$_GET['idtodo']
					);
					header("Location: ".$_SERVER['PHP_SELF']);
					return 0;

				case 'filter':
					if ( ! isset($_GET['by']) ) { break ; }
					switch ( $_GET['by'] )
					{
						case 'priority':
							if(!isset($_GET['level'])){break;}
							$this->myObjects['Cookie']->clearfilters();
							$this->myObjects['Cookie']->setFilterBy("priority");
							$this->myObjects['Cookie']->setFilterLv($_GET['level']);
							$this->myObjects['Cookie']->saveCookie();
							header("Location: ".$_SERVER['PHP_SELF']);
							return 0;

						case 'undone':
							$this->myObjects['Cookie']->clearfilters();
							$this->myObjects['Cookie']->setFilterBy("undone");
							$this->myObjects['Cookie']->saveCookie();
							header("Location: ".$_SERVER['PHP_SELF']);
							return 0;

						case 'done':
							$this->myObjects['Cookie']->clearfilters();
							$this->myObjects['Cookie']->setFilterBy("done");
							$this->myObjects['Cookie']->saveCookie();
							header("Location: ".$_SERVER['PHP_SELF']);
							return 0;

						case 'label':
							if ( ! isset ( $_GET['id'] ) ) { break; }
							$this->myObjects['Cookie']->clearfilters();
							$this->myObjects['Cookie']->setFilterBy("label");
							$this->myObjects['Cookie']->setFilterId($_GET['id']);
							$this->myObjects['Cookie']->saveCookie();
							header("Location: ".$_SERVER['PHP_SELF']);
							return 0;

						default:
							// unknown filter, do nothing
					}
					break;

				case 'reset':
					$this->myObjects['Cookie']->clearfilters();
					$this->myObjects['Cookie']->saveCookie();
					header("Location: ".$_SERVER['PHP_SELF']);
					return 0;

				case 'modify': // just fill addTodo form with hidden field "update"
					if ( ! isset ( $_GET['idtodo'] ) ) { break; } 
					/* shortcuts */
					$O = $this->myObjects;
					$username = $O['Cookie']->getUsername();
					$idtodo   = $_GET['idtodo'];
					/* security test */
					if ( ! $O['DB']->todoExists($username, $idtodo) )
					{
						echo "<h1>Updating a todo failed!</h1>\n";
						echo "<p>Your username doesn't match with identificator ";
						echo "of todo to be updated. That means: either you triying ";
						echo "to do something bad or application horribly failed. ";
						echo "In second case, we are sorry. If you see this page not for ";
						echo "first time, please, <a href=\"mailto:martin@kopta.eu\">";
						echo "contact the administrator</a>.</p>\n";
						echo "<p>Continue to <a href=\"".$_SERVER['PHP_SELF']."\">main page</a></p>";
						return 0;
					}
					/* hidden field */
					$O['View']->set("idtodo", $idtodo);
					/* labels field */
					$ret = $O['DB']->getTodoLabels($idtodo);
					if ( ! empty( $ret ) )
					{
						$labels = join (", ", $ret);
						$O['View']->set("labelsField", $labels);
					}
					/* priority select */
					$ret = $O['DB']->getTodoPriority($idtodo);
					if ( ! empty ( $ret ) )
					{
						$O['View']->set("priority", $ret);
					}
					/* deadline selectors */
					$ret = $O['DB']->getTodoDeadline($idtodo);
					if ( ! empty ( $ret ) )
					{
						$dl = explode( "-", $ret); // ex.: 2000-12-24
						$O['View']->set("selectedYear",  $dl[0]);
						$O['View']->set("selectedMonth", $dl[1]);
						$O['View']->set("selectedDay",   $dl[2]);
					}
					/* text of todo */
					$ret = $O['DB']->getTodoText($idtodo);
					$O['View']->set("todoTextArea", $ret);
					$this->showMainPage();
					return 0;
					break;

				default:
					// unknown action, do nothing
			}
		}
		

		if ( isset($_POST['addTodo']) )
		{
			if ( empty ($_POST['todoTextArea'] ) )
			{
				$error = "You forget to write your todo.";
				$this->myObjects['View']->set
				(
					"labelsField",
					$_POST['labelsField']
				);
				$this->myObjects['View']->set
				(
					"priority",
					$_POST['prioritySelect']
				);
				if ( $_POST['dlY'] != '---' )
				{
					$this->myObjects['View']->set
					(
						"selectedYear",
						$_POST['dlY']
					);
				}
				if ( $_POST['dlM'] != '---' )
				{
					$this->myObjects['View']->set
					(
						"selectedMonth",
						$_POST['dlM']
					);
				}
				if ( $_POST['dlD'] != '---' )
				{
					$this->myObjects['View']->set
					(
						"selectedDay",
						$_POST['dlD']
					);
				}
				$this->myObjects['View']->set("addTodoError", $error);
				$this->myObjects['View']->set("idtodo", $_POST['update']);
				$this->showMainPage();
				return 0;
			}

			if ( $_POST['dlY'] == '---' or
				$_POST['dlM'] == '---' or
				$_POST['dlD'] == '---' )
			{
				$deadline = null;	
			}
			else
			{
				$deadline = $_POST['dlY']."-".$_POST['dlM']."-".$_POST['dlD'];
			}

			{ // pridani todo
				$labels = array();
				if ( ! empty ( $_POST['labelsField'] ) )
				{
					$labels = explode(",", $_POST['labelsField']);
					foreach ( $labels as &$label )
					{
						$label = trim( $label );
					}
				}

				if ( $_POST['update'] != -1 )
				{
					if ( !
						$this->myObjects['DB']->todoExists
						(
							$this->myObjects['Cookie']->getUsername(),
							$_POST['update'])
						)
					{
						// error in input data (security error)
						echo "<h1>Updating a todo failed!</h1>\n";
						echo "<p>Your username doesn't match with identificator ";
						echo "of todo to be updated. That means: either you triying ";
						echo "to do something bad or application horribly failed. ";
						echo "In second case, we are sorry. If you see this page not for ";
						echo "first time, please, <a href=\"mailto:martin@kopta.eu\">";
						echo "contact the administrator</a>.</p>\n";
						echo "<p>Continue to <a href=\"".$_SERVER['PHP_SELF']."\">main page</a></p>";
						return 0;
					}
					if ( !
						$this->myObjects['DB']->removeTodo
						(
							$this->myObjects['Cookie']->getUsername(),
							$_POST['update']
						)
					)
					{
						// error while removing old todo
					echo "<h1>Removing todo failed!</h1>\n";
					echo "<p>Query to database failed. Todo is not removed. Try it again and ";
					echo "if problem is persistent, please <a href=\"mailto:martin@kopta.eu\">";
					echo "contact the administrator</a>.</p>\n";
					echo "<p>Continue to <a href=\"".$_SERVER['PHP_SELF']."\">main page</a></p>";
					return 0;
					}
				}

				if ( !
					$this->myObjects['DB']->addTodo
					(
						$this->myObjects['Cookie']->getUsername(),
						$_POST['prioritySelect'],
						$deadline,
						$_POST['todoTextArea'],
						$labels
					)
				)
				{
					echo "<h1>Adding todo failed!</h1>\n";
					echo "<p>Query to database failed. Todo is not added. Try it again and ";
					echo "if problem is persistent, please <a href=\"mailto:martin@kopta.eu\">";
					echo "contact the administrator</a>.</p>\n";
					echo "<p>We apologise for your trouble.</p>\n";
					echo "<p>Continue to <a href=\"".$_SERVER['PHP_SELF']."\">main page</a></p>";
					return 0;
				}
				header("Location: ".$_SERVER['PHP_SELF']);
				return 0;
			}
		}

		if ( isset($_POST['chpasswd']) )
		{
			$error = null;
			if ( empty($_POST['oldpass']) )
			{ $error = "You have to enter your old password."; }
			else if ( empty($_POST['newpass1']) )
			{ $error = "You have to enter your new password."; }
			else if ( empty($_POST['newpass2']) )
			{ $error = "You have to enter your new password twice."; }
			else if ( $_POST['newpass1'] != $_POST['newpass2'] )
			{ $error = "New passwords doesn't mach each other."; }
			else if ( ! eregi('^.{4,50}$', $_POST['newpass1']) )
			{
				$error  = "New password must be at least ";
				$error .= "4 characters long (max 50).";
			}
			else if ( ! 
				$this->myObjects['DB']->checkAuth
				(
					$this->myObjects['Cookie']->getUsername(),
					$this->ha($_POST['oldpass'])
				)
			)
			{
				$error  = "Old password is not correct. (If you have trouble ";
				$error .= "to remmember your old password, try to search thru";
				$error .= " you browser memory or <a href=\"mailto:martin@kopta.eu\">";
				$error .= "contact the administrator</a>.)";
			}

			if ( isset($error) )
			{
				$this->myObjects['View']->set
				(
					"prefix",
					$this->app_styledir."/".$this->app_style."/"
				);
				$this->myObjects['View']->set
				(
					"pageTitle",
					"Metodo 2 - web todo"
				);
				$this->myObjects['View']->set
				(
					"bottomText",
					"&copy; Martin Kopta, 2008"
				);
				$this->myObjects['View']->set("errorText", $error);
				$this->myObjects['View']->set
				(
					"username",
					$this->myObjects['Cookie']->getUsername()
				);
				$this->myObjects['View']->show('chpasswd.php');
				return 0;
			}
			else
			{
				// Zmena hesla
				$this->myObjects['DB']->setPass
				(
					$this->myObjects['Cookie']->getUsername(),
					$this->ha($_POST['newpass1'])
				);
				$this->myObjects['Cookie']->setPasshash
				(
					$this->ha($_POST['newpass1'])
				);
				$this->myObjects['Cookie']->saveCookie();

				header("location: ".$_SERVER['PHP_SELF']);
				return 0;
			}
		}

		// ABSOLUT DEFAULT
		$this->showMainPage();
	}

}

/* EOF */ ?>
