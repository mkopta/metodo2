<?php

if (!defined("INDEX"))
{
	// This file is called in wrong way!
	echo "You probably don't have to be here. Please, go back.";
	die();
}


class ccMrNoCandy extends ccSkeleton
{
	private $myObjects;
	private $cookie_name;
	private $app_styledir;
	private $app_style;
	private $db_server;
	private $db_database;
	private $db_username;
	private $db_password;

	public function __construct
	(
		$cookie_name, $app_styledir, $app_style,
		$db_server, $db_database, $db_username, $db_password
	)
	{
		$this->myObjects = array();
		$this->cookie_name  = $cookie_name;
		$this->app_styledir = $app_styledir;
		$this->app_style    = $app_style;
		$this->db_server    = $db_server;
		$this->db_database  = $db_database;
		$this->db_username  = $db_username;
		$this->db_password  = $db_password;
	}


	private function showStandardLoginPage()
	{
		$myObjects = $this->myObjects;
		$myObjects['View']->set
		(
			"prefix",
			$this->app_styledir."/".$this->app_style."/"
		);
		$myObjects['View']->set("pageTitle", "Metodo 2 - web todo");
		$myObjects['View']->set("bottomText", "&copy; Martin Kopta, 2008");
		$myObjects['View']->show('loginpage.php');
		return 0;
	}


	public function start()
	{
		// Vytvoreni objektu pracujicim se susenkami
		$this->myObjects['Cookie'] = new cCookie($this->cookie_name);
		// Vytvoreni objektu zprostredkovavajim zobrazovani
		$this->myObjects['View']   = new cView();

		if ( ! empty ( $_POST ) ) // Prisla data z formulare (login nebo registrace)
		{
			if ( isset ( $_POST['loginButton'] ) ) // Jsou to data z loginu
			{
				// Jestlize vsecko je vyplnene, vytvorime susenku a reloadujeme
				if (
					!empty($_POST['usernameField']) &&
					!empty($_POST['passwordField'])
				)
				{
					$this->myObjects['Cookie']->setUsername
					(
						$_POST['usernameField']
					);
					$this->myObjects['Cookie']->setPasshash
					(
						$this->ha($_POST['passwordField'])
					);
					$this->myObjects['Cookie']->saveCookie();
					// Refresh !!
					header("location: ".$_SERVER['PHP_SELF']);
					return 0;
				}
				// Neco uzivatel nezadal
				else
				{
					$error = null;
					// Tady overim co presne bylo spatne a vypisu hlasku a
					// zobrazim login page s tou chybovou hlaskou

					// Uzivatel nezadal ani heslo ani jmeno (to je ale debil)
					if (
						empty($_POST['usernameField']) &&
						empty ($_POST['passwordField'])
					)
					{
						$this->myObjects['View']->set
						(
							"errorText",
							"Username and password is missing!"
						);
					}
					// Uzivatel zapomel jenom username
					else if ( empty($_POST['usernameField']) )
					{
						$this->myObjects['View']->set
						(
							"errorText",
							"Username is missing!"
						);
					}
					// Uzivatel zapomel jenom heslo
					else if ( empty($_POST['passwordField']) )
					{
						$this->myObjects['View']->set
						(
							"username",
							$_POST['usernameField']
						);
						$this->myObjects['View']->set
						(
							"errorText",
							"Password is missing!"
						);
					}

					$this->showStandardLoginPage();
					return 0;
				}
			}
			else if ( isset ( $_POST['registerButton'] ) ) // Jsou to data z registrace
			{
				// nazhavim databazove pripojeni
				{
					$this->myObjects['DB'] = new cDatabase($this->db_server,
					                                      $this->db_username,
					                                      $this->db_password,
					                                      $this->db_database); 
					// pokud se pripojim uspesne
					if ( $this->myObjects['DB']->isError )
					{
						echo "<h1>Dabase error!</h1>\n";
						echo "<p>Connection to database failed, please check the \n";
						echo "configuration in file 'configuration.php'.\n</p></p>In ";
						echo "case of trouble, read the <a href=\"doc/\">";
						echo "documentation</a>.</p><p>If you lost any input or data, ";
						echo "we are terribly sorry, please don't get mad at us.</p>";
						return 0;
					}
				}
				// kontrolovani dat z formulare
				{
					// Chybove hlasky
					$usernameFieldError = null;
					$passwordFieldError = null;
					$passwordFieldError = null;
					$antisp1Error = null;
					$antisp2Error = null;
					$antisp3Error = null;
					$error = 0;

					// Tady zpracuji data z registrace
					// Rozhodnu jestli jsou dobra a pokud ano, zobrazim neco o
					// tom ze je vsecko fajn. jinak vyhodim zpatky registration
					// page s predvyplnenymi hodnotami a hlaskou co bylo spatne

					// Username checks
					if ( empty($_POST['usernameField']) )
					{
						$usernameFieldError = "Username must be chosen.";
						$error = 1;
					}
					else if ( ! eregi('^[-_a-z0-9]{2,32}$', $_POST['usernameField']) )
					{
						$usernameFieldError  = "Username must be from 2 to ";
						$usernameFieldError .= "32 characters long and ";
						$usernameFieldError .= "can contain only alphanumeric characters ";
						$usernameFieldError .= "and \"_\" and \"-\".";
						$error = 1;
					} 
					else if ( $this->myObjects['DB']->userExists($_POST['usernameField']) )
					{
						$usernameFieldError = "This username is already taken by someone else. ";
						$usernameFieldError .= "Please chose another one.";
						$error = 1;
					} // ELSE OK

					// Password checks
					if ( empty($_POST['passwordField']) and empty($_POST['passwordField2']) )
					{
						$passwordFieldError = "Password must be chosen.";
						$error = 1;
					}
					else if ( $_POST['passwordField'] != $_POST['passwordField2'] )
					{
						$passwordFieldError = "Passwords doesn't match each other.";
						$error = 1;
					}
					else if ( ! eregi('^.{4,50}$', $_POST['passwordField']) )
					{
						$passwordFieldError = "Password must be at least 4 characters long (max 50).";
						$error = 1;
					} // ELSE OK

					// Antispam checks
					// antisp1
					if ( empty($_POST['antisp1']) )
					{
						$antisp1Error = "You don't answered the question.";
						$error = 1;
					}
					else if ( ! eregi('^red$', $_POST['antisp1']) )
					{
						$antisp1Error = "You don't answered the question right. Please, try it again..";
						$error = 1;
					}
					// antisp2
					if ( empty($_POST['antisp2']) )
					{
						$antisp2Error = "You don't answered the question.";
						$error = 1;
					}
					else if ( ! eregi('^n[oO]+[!]*$', $_POST['antisp2']) )
					{
						$antisp2Error = "You don't answered the question right. Please, try it again..";
						$error = 1;
					}
					// antisp3
					if ( empty($_POST['antisp3']) )
					{
						$antisp3Error = "You don't answered the question.";
						$error = 1;
					}
					else if ( ! eregi('^cake[!]*$', $_POST['antisp3']) )
					{
						$antisp3Error = "You don't answered the question right. Please, try it again..";
						$error = 1;
					}


					// Jestlize zpracovani formaulare pro registraci zahlasilo chybu, vratime formular
					// uzivateli zpatky s hodnotami a rekneme kde udelal chyby
					if ( $error == 1 )
					{
						// Zakladni informace stranky
						$this->myObjects['View']->set("prefix", $this->app_styledir."/".$this->app_style."/");
						$this->myObjects['View']->set("pageTitle", "Metodo 2 - web todo");
						$this->myObjects['View']->set("bottomText", "&copy; Martin Kopta, 2008");
						// Nastaveni chybovych hlasek
						$this->myObjects['View']->set('usernameFieldError', $usernameFieldError);
						$this->myObjects['View']->set('passwordFieldError', $passwordFieldError);
						$this->myObjects['View']->set('antisp1Error', $antisp1Error);
						$this->myObjects['View']->set('antisp2Error', $antisp2Error);
						$this->myObjects['View']->set('antisp3Error', $antisp3Error);
						// Vraceni uzivatelovych hodnot
						$this->myObjects['View']->set('usernameField', $_POST['usernameField']);
						$this->myObjects['View']->set('firstnameField', $_POST['firstnameField']);
						$this->myObjects['View']->set('lastnameField', $_POST['lastnameField']);
						$this->myObjects['View']->set('contactField', $_POST['contactField']);
						$this->myObjects['View']->set('antisp1', $_POST['antisp1']);
						$this->myObjects['View']->set('antisp2', $_POST['antisp2']);
						$this->myObjects['View']->set('antisp3', $_POST['antisp3']);
						// Vygenerovani stranky
						$this->myObjects['View']->show('registration.php');
						// Konec controlleru
						return 0;
					}

				} // Dobehly vsechny kontroly registracniho formulare a vsechno je v poradku
				// zpracovani dat z formulare
				{
					// tak se pokusim registrovat
					$rv = $this->myObjects['DB']->createUser
					(
						$_POST['usernameField'],
						$this->ha($_POST['passwordField']),
						$_POST['firstnameField'],
						$_POST['lastnameField'],
						$_POST['contactField']
					);

					if (!$rv)
					{
						echo "<h1>Dabase error!</h1>\n";
						echo "<p>Connection to database failed, please check the \n";
						echo "configuration in file 'configuration.php'.\n</p></p>In ";
						echo "case of trouble, read the <a href=\"doc/\">";
						echo "documentation</a>.</p>\n";
						echo "<p>Your registration has failed :-(</p>";
						return 0;
					}	

					// add users first todos
					$rv = $this->myObjects['DB']->addTodo
					(
						$_POST['usernameField'],
						'1',
						null,
						'Welcome to metodo!',
						array('metodo', 'this')
					);
					if (!$rv)
					{
						// Error while adding new todo. Since it is not critical NOW,
						// we do nothing
					}

					$rv = $this->myObjects['DB']->addTodo
					(
						$_POST['usernameField'],
						'4',
						null,
						'Read the documentation of metodo.',
						array('metodo', 'this', 'rtfm', 'reading')
					);
					if (!$rv)
					{
						// Error while adding new todo. Since it is not critical NOW,
						// we do nothing
					}
						
					// Dokonceni registrace

					// Ulozim jmeno a heslo do susenky a tak se uzivatel nebude muset prihlasovat
					$this->myObjects['Cookie']->setUsername($_POST['usernameField']);
					$this->myObjects['Cookie']->setPasshash($this->ha($_POST['passwordField']));
					$this->myObjects['Cookie']->saveCookie();
					// Refresh !!
					header("location: ".$_SERVER['PHP_SELF']);

					return 0; // A zili stastne az do smrti
				}
			}
		}
		else if ( ! empty ( $_GET ) ) // Uzivatel nam dal nejakou informaci skrz URL
		{
			if ( isset ( $_GET['section'] ) )
			{
				if ( $_GET['section'] == "registration" )
				{
					$this->myObjects['View']->set
					(
						"prefix",
						$this->app_styledir."/".$this->app_style."/"
					);
					$this->myObjects['View']->set("pageTitle", "Metodo 2 - web todo");
					$this->myObjects['View']->set
					(
						"bottomText",
						"&copy; Martin Kopta, 2008"
					);
					$this->myObjects['View']->show('registration.php');
					return 0;
				}
				else
				{
					$this->showStandardLoginPage();
					return 0;
				}
			}
			else
			{
				$this->showStandardLoginPage();
				return 0;
			}
		}
		else // Od uzivatele jsme nic nedostali a o uzivateli nic nevime
			// -> zobrazime prihlasovaci obrazovku
		{
			$this->showStandardLoginPage();
			return 0;
		}
	}

}

/* EOF */ ?>
